provider "aws" {
  region = "us-east-1"
}
variable "region" {
     default = "us-east-1"
}
variable "availabilityZone" {
     default = "us-east-1a"
}
variable "instanceTenancy" {
    default = "default"
}
variable "vpcCIDRblock" {
    default = "50.0.0.0/16"
}
variable "subnet1CIDRblock" {
    default = "50.0.1.0/24"
}
variable "subnet2CIDRblock" {
    default = "50.0.2.0/24"
}

# create the VPC
resource "aws_vpc" "simplevpc" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy
tags = {
    Name = "Automation-VPC1"
}
}



# create the pub Subnet
resource "aws_subnet" "pubsnet" {
  vpc_id                  = aws_vpc.simplevpc.id
  cidr_block              = var.subnet1CIDRblock
  availability_zone       = var.availabilityZone
tags = {
   Name = "Automation-PUB-SN1"
}
}

# create the pri Subnet
resource "aws_subnet" "prisnet" {
  vpc_id                  = aws_vpc.simplevpc.id
  cidr_block              = var.subnet2CIDRblock
  availability_zone       = var.availabilityZone
tags = {
   Name = "Automation-PRI-SN1"
}

}


 

output "vpc_id" {
  value = aws_vpc.simplevpc.id
}

output "public_snet_id" {
  value = aws_subnet.pubsnet.id
}

output "private_snet_id" {
  value = aws_subnet.prisnet.id
}